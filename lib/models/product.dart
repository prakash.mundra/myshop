class Product {
  String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavourite;

  Product(
      {required this.id,
      required this.title,
      required this.description,
      required this.price,
      required this.imageUrl,
      this.isFavourite = false});

  Map<String, dynamic> toJSON(String? userId) {
    return {
      'title': title,
      'description': description,
      'price': price,
      'imageUrl': imageUrl,
      'creatorId': userId
    };
  }

  Map<String, dynamic> toJSONWithId() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'price': price,
      'imageUrl': imageUrl
    };
  }
}
