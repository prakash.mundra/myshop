class AuthData {
  String? token;
  DateTime? expiryTime;
  String? userId;

  AuthData({this.token, this.expiryTime, this.userId});

  Map<String, dynamic> toJSON() {
    return {
      'token': token,
      'expiryTime': expiryTime?.toIso8601String(),
      'userId': userId,
    };
  }
}
