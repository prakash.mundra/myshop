import '/models/product.dart';

class CartItem {
  final String id;
  final int quantity;
  final Product product;

  CartItem({required this.id, required this.quantity, required this.product});

  Map<String, dynamic> toJSON() {
    return {
      'id': id,
      'quantity': quantity,
      'product': product.toJSONWithId(),
    };
  }
}
