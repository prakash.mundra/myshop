import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/auth.dart' as auth_provider;
import '/providers/cart.dart' as cart_provider;
import '/providers/orders.dart' as order_provider;
import '/providers/products.dart' as products_provider;
import '/screens/auth.dart';
import '/screens/cart.dart';
import '/screens/edit_product.dart';
import '/screens/orders.dart';
import '/screens/product_details.dart';
import '/screens/products_overview.dart';
import '/screens/user_products.dart';
import 'helpers/custom_route.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => auth_provider.Auth()),
        ChangeNotifierProxyProvider<auth_provider.Auth,
                products_provider.Products>(
            create: (context) => products_provider.Products(),
            update: (_, auth, products) =>
                products!..onProxyUpdate(auth.getAuthData())),
        ChangeNotifierProvider(create: (context) => cart_provider.Cart()),
        ChangeNotifierProxyProvider<auth_provider.Auth, order_provider.Orders>(
            create: (context) => order_provider.Orders(),
            update: (_, auth, orders) =>
                orders!..onProxyUpdate(auth.getAuthData())),
      ],
      child: Consumer<auth_provider.Auth>(
        builder: (bContext, auth, child) {
          return MaterialApp(
            title: 'MyShop',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.purple)
                  .copyWith(secondary: Colors.deepOrange),
              fontFamily: 'Lato',
              pageTransitionsTheme: PageTransitionsTheme(builders: {
                TargetPlatform.android: CustomPageTransitionsBuilder(),
                TargetPlatform.iOS: CustomPageTransitionsBuilder()
              }),
            ),
            home: auth.isAuthenticated()
                ? const ProductOverview()
                : FutureBuilder(
                    future: auth.autoLogin(),
                    builder: (bContext, dataSnapshot) {
                      if (dataSnapshot.connectionState ==
                          ConnectionState.waiting) {
                        return const SizedBox();
                      }
                      return const Auth();
                    }),
            routes: {
              ProductDetails.routeName: (context) => const ProductDetails(),
              Cart.routeName: (context) => const Cart(),
              Orders.routeName: (context) => const Orders(),
              UserProducts.routeName: (context) => const UserProducts(),
              EditProduct.routeName: (context) => const EditProduct(),
            },
          );
        },
      ),
    );
  }
}
