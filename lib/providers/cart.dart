import 'package:flutter/material.dart';

import '/models/cart_item.dart';
import '/models/product.dart';

class Cart extends ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> getItems() {
    return {..._items};
  }

  void addItem(String id, Product product) {
    if (_items.containsKey(id)) {
      _items.update(id, (existingItem) {
        return CartItem(
            id: existingItem.id,
            quantity: existingItem.quantity + 1,
            product: existingItem.product);
      });
    } else {
      _items.putIfAbsent(id, () {
        return CartItem(
            id: DateTime.now().toString(), quantity: 1, product: product);
      });
    }
    notifyListeners();
  }

  int getItemCount() {
    return _items.length;
  }

  double getTotalAmount() {
    var total = 0.0;
    _items.forEach((key, value) {
      total += (value.product.price * value.quantity);
    });
    return double.parse(total.toStringAsFixed(2));
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeItemQuantity(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }
    if (_items[productId]!.quantity > 1) {
      _items.update(
          productId,
          (existingItem) => CartItem(
              id: existingItem.id,
              quantity: (existingItem.quantity - 1),
              product: existingItem.product));
    } else {
      _items.remove(productId);
    }
    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}
