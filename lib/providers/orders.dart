import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_shop/models/product.dart';

import '../models/auth_data.dart';
import '../models/cart_item.dart';
import '../models/order.dart';

class Orders extends ChangeNotifier {
  AuthData? _authData;
  List<Order> _orders = [];

  void onProxyUpdate(AuthData? authData) {
    _authData = authData;
    if (authData == null) {
      _orders.clear();
    }
    notifyListeners();
  }

  Future<void> fetchOrders() async {
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/orders/${_authData?.userId}.json?auth=${_authData?.token}';
    final response = await http.get(Uri.parse(url));
    final responseBody = json.decode(response.body);
    final List<Order> orders = [];
    if (responseBody != null) {
      final responseData = responseBody as Map<String, dynamic>;
      responseData.forEach((key, value) {
        orders.add(Order(
          id: key,
          amount: value['amount'],
          dateTime: DateTime.parse(value['dateTime']),
          items: (value['items'] as List<dynamic>).map((item) {
            final product = item['product'];
            return CartItem(
                id: item['id'],
                quantity: item['quantity'],
                product: Product(
                  id: product['id'],
                  title: product['title'],
                  description: product['description'],
                  price: product['price'],
                  imageUrl: product['imageUrl'],
                ));
          }).toList(),
        ));
      });
    }
    _orders = orders;
    notifyListeners();
  }

  List<Order> getOrders() {
    return [..._orders];
  }

  Future<void> addOrder(List<CartItem> items, double total) async {
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/orders/${_authData?.userId}.json?auth=${_authData?.token}';
    final order = Order(
        id: DateTime.now().toString(),
        amount: total,
        items: items,
        dateTime: DateTime.now());
    final response =
        await http.post(Uri.parse(url), body: json.encode(order.toJSON()));
    order.id = json.decode(response.body)['name'];
    _orders.insert(0, order);
    notifyListeners();
  }
}
