import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/auth_data.dart';
import '../models/http_exception.dart';
import '../models/product.dart';

class Products with ChangeNotifier {
  AuthData? _authData;
  List<Product> _products = [];

  void onProxyUpdate(AuthData? authData) {
    _authData = authData;
    if (authData == null) {
      _products.clear();
    }
    notifyListeners();
  }

  Future<void> fetchProducts([bool filterByUser = false]) async {
    final filterQuery = filterByUser
        ? '&orderBy="creatorId"&equalTo="${_authData?.userId}"'
        : '';
    var url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/products.json?auth=${_authData?.token}$filterQuery';
    final response = await http.get(Uri.parse(url));
    final responseBody = json.decode(response.body);
    final List<Product> products = [];
    if (responseBody != null) {
      url =
          'https://my-shop-6b9f8-default-rtdb.firebaseio.com/favoutires/${_authData?.userId}.json?auth=${_authData?.token}';
      final responseData = responseBody as Map<String, dynamic>;
      final favResponse = await http.get(Uri.parse(url));
      final favResponseData = json.decode(favResponse.body);
      responseData.forEach((productId, product) {
        products.add(Product(
          id: productId,
          title: product['title'],
          description: product['description'],
          price: product['price'],
          imageUrl: product['imageUrl'],
          isFavourite: favResponseData != null
              ? (favResponseData[productId] ?? false)
              : false,
        ));
      });
    }
    _products = products;
    notifyListeners();
  }

  List<Product> getProducts() {
    return [..._products];
  }

  List<Product> getFavoriteProducts() {
    return _products.where((product) => product.isFavourite).toList();
  }

  Product getProductById(String id) {
    return _products.firstWhere((product) => product.id == id);
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/products.json?auth=${_authData?.token}';
    final response = await http.post(Uri.parse(url),
        body: json.encode(product.toJSON(_authData?.userId)));
    product.id = json.decode(response.body)['name'];
    _products.add(product);
    notifyListeners();
  }

  Future<void> updateProduct(int index, Product product) async {
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/products/${product.id}.json?auth=${_authData?.token}';
    await http.patch(Uri.parse(url),
        body: json.encode(product.toJSON(_authData?.userId)));
    _products[index] = product;
    notifyListeners();
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/products/$id.json?auth=${_authData?.token}';
    final existingIndex = getProductIndexById(id);
    final existingProduct = _products[existingIndex];
    _products.removeWhere((product) => product.id == id);
    notifyListeners();
    final response = await http.delete(Uri.parse(url));
    if (response.statusCode != 200) {
      _products.insert(existingIndex, existingProduct);
      notifyListeners();
      throw HttpException('Could not deleted');
    }
  }

  int getProductIndexById(String id) {
    return _products.indexWhere((product) => product.id == id);
  }

  Future<void> toggleFavourite(String id) async {
    final product = getProductById(id);
    final existingStatus = product.isFavourite;
    product.isFavourite = !existingStatus;
    notifyListeners();
    final url =
        'https://my-shop-6b9f8-default-rtdb.firebaseio.com/favoutires/${_authData?.userId}/${product.id}.json?auth=${_authData?.token}';
    final response =
        await http.put(Uri.parse(url), body: json.encode(product.isFavourite));
    if (response.statusCode != 200) {
      product.isFavourite = existingStatus;
      notifyListeners();
      throw HttpException('Could not success');
    }
  }
}
