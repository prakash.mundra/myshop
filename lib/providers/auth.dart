import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/auth_data.dart';
import '../models/http_exception.dart';

const String prefAuthDataKey = 'auth_data';

class Auth extends ChangeNotifier {
  AuthData? _authData;
  Timer? _authTimer;

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url =
        'https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=AIzaSyAnqDs_EL6t_jgrsg9j0JgOrnVtflA5hL4';
    final response = await http.post(Uri.parse(url),
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ));
    final responseData = json.decode(response.body);
    final error = responseData['error'];
    if (error != null) {
      final errors = error['errors'];
      final finalError = errors.map((error) => error['message']).join(' ');
      throw HttpException(finalError);
    } else {
      _authData = AuthData(
        token: responseData['idToken'],
        userId: responseData['localId'],
        expiryTime: DateTime.now()
            .add(Duration(seconds: int.parse(responseData['expiresIn']))),
      );
      await savePreferences(_authData);
      autoLogout();
      notifyListeners();
    }
  }

  Future<void> signUp(String email, String password) async {
    return _authenticate(email, password, 'signUp');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'signInWithPassword');
  }

  Future<void> savePreferences(AuthData? authData) async {
    final preferences = await SharedPreferences.getInstance();
    preferences.setString(prefAuthDataKey, json.encode(_authData?.toJSON()));
  }

  Future<bool> autoLogin() async {
    final preferences = await SharedPreferences.getInstance();
    if (!preferences.containsKey(prefAuthDataKey)) {
      return false;
    }
    final data = json.decode(preferences.getString(prefAuthDataKey)!);
    final expiryTime = DateTime.parse(data['expiryTime']);
    if (expiryTime.isBefore(DateTime.now())) {
      return false;
    }
    _authData = AuthData(
      token: data['token'],
      expiryTime: expiryTime,
      userId: data['userId'],
    );
    autoLogout();
    notifyListeners();
    return true;
  }

  bool isAuthenticated() {
    return _getToken() != null;
  }

  String? _getToken() {
    if (_authData?.token != null &&
        _authData?.expiryTime != null &&
        _authData!.expiryTime!.isAfter(DateTime.now())) {
      return _authData?.token;
    }
    return null;
  }

  AuthData? getAuthData() {
    return _authData;
  }

  Future<void> logout() async {
    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }
    _authData = null;
    final preferences = await SharedPreferences.getInstance();
    preferences.clear();
    notifyListeners();
  }

  void autoLogout() {
    if (_authData?.expiryTime != null) {
      final timeToTokenExpiry =
          _authData!.expiryTime!.difference(DateTime.now()).inSeconds;
      _authTimer = Timer(Duration(seconds: timeToTokenExpiry), logout);
    }
  }
}
