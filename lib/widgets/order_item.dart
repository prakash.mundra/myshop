import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/order.dart';

class OrderItem extends StatefulWidget {
  final Order order;

  const OrderItem(this.order, {super.key});

  @override
  State<OrderItem> createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  var _expanded = false;

  @override
  Widget build(BuildContext context) {
    final orderItems = widget.order.items;
    return Card(
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [
          ListTile(
            title: Text('\$${widget.order.amount}'),
            subtitle: Text(
                DateFormat('dd/MM/yyyy hh:mm').format(widget.order.dateTime)),
            trailing: IconButton(
              icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
              onPressed: () {
                setState(() {
                  _expanded = !_expanded;
                });
              },
            ),
          ),
          AnimatedContainer(
            constraints: BoxConstraints(
              minHeight: _expanded ? (orderItems.length * 10) : 0,
              maxHeight: _expanded ? (orderItems.length * 60) : 0,
            ),
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeInOut,
            child: Visibility(
              visible: _expanded,
              child: ListView.builder(
                  itemBuilder: (_, index) {
                    return ListTile(
                        title: Text(orderItems[index].product.title),
                        trailing: Text(
                            '\$${orderItems[index].product.price} x ${orderItems[index].quantity}'));
                  },
                  itemCount: orderItems.length,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics()),
            ),
          ),
        ],
      ),
    );
  }
}
