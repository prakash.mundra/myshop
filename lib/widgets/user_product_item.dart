import 'package:flutter/material.dart';

import '../models/product.dart';
import '../screens/edit_product.dart';

class UserProductItem extends StatelessWidget {
  final Product product;
  final Function onDelete;

  const UserProductItem(this.product, this.onDelete, {super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ListTile(
      title: Text(product.title),
      leading: CircleAvatar(backgroundImage: NetworkImage(product.imageUrl)),
      trailing: FittedBox(
          child: Row(
        children: [
          IconButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(EditProduct.routeName, arguments: product.id);
              },
              icon: Icon(
                Icons.edit,
                color: theme.colorScheme.primary,
              )),
          IconButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (bContext) {
                      return AlertDialog(
                        title: const Text('Are you sure?'),
                        content:
                            const Text('Do you want to remove the Product?'),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(bContext).pop(false);
                              },
                              child: const Text('No')),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop(true);
                                onDelete(product.id);
                              },
                              child: const Text('Yes')),
                        ],
                      );
                    });
              },
              icon: Icon(Icons.delete, color: theme.colorScheme.error))
        ],
      )),
    );
  }
}
