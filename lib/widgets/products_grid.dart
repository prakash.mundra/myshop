import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';
import '../widgets/product_item.dart';

class ProductsGrid extends StatelessWidget {
  final bool _showFavourites;

  const ProductsGrid(this._showFavourites, {super.key});

  @override
  Widget build(BuildContext context) {
    final productsProvider = context.watch<Products>();
    final products = _showFavourites
        ? productsProvider.getFavoriteProducts()
        : productsProvider.getProducts();
    return GridView.builder(
        padding: const EdgeInsets.all(10),
        itemCount: products.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
        itemBuilder: (context, index) {
          return ProductItem(products[index]);
        });
  }
}
