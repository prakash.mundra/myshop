import 'package:flutter/material.dart';
import 'package:my_shop/widgets/my_dialogs.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';

enum AuthMode { signUp, login }

class AuthCard extends StatefulWidget {
  const AuthCard({super.key});

  @override
  State createState() {
    return _AuthCardState();
  }
}

class _AuthCardState extends State<AuthCard>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  var _authMode = AuthMode.login;
  final Map<String, String> _authData = {
    'email': '',
    'password': '',
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _showLoading(bool show) {
    setState(() {
      _isLoading = show;
    });
  }

  void _onModeChange() {
    setState(() {
      _authMode =
          (_authMode == AuthMode.login) ? AuthMode.signUp : AuthMode.login;
    });
  }

  void _onSubmit() async {
    final isValid = _formKey.currentState?.validate();
    if (isValid == true) {
      _formKey.currentState?.save();
      _showLoading(true);
      try {
        if (_authMode == AuthMode.login) {
          await context
              .read<Auth>()
              .login(_authData['email']!, _authData['password']!);
        } else {
          await context
              .read<Auth>()
              .signUp(_authData['email']!, _authData['password']!);
        }
        _showLoading(false);
      } catch (e) {
        _showError(e.toString());
      }
    }
  }

  void _showError(String message) {
    _showLoading(false);
    MyDialog.showErrorDialog(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Stack(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 5,
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(children: [
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Email'),
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value!.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _authData['email'] = value!;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Password'),
                      obscureText: true,
                      controller: _passwordController,
                      validator: (value) {
                        if (value!.isEmpty || value.length < 5) {
                          return 'Password should be at least 6 characters';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _authData['password'] = value!;
                      },
                    ),
                    AnimatedContainer(
                      constraints: BoxConstraints(
                        minHeight: _authMode == AuthMode.login ? 0 : 60,
                        maxHeight: _authMode == AuthMode.login ? 0 : 100,
                      ),
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeInOut,
                      child: AnimatedOpacity(
                        opacity: _authMode == AuthMode.login ? 0 : 1.0,
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.easeInOut,
                        child: TextFormField(
                          decoration: const InputDecoration(
                              labelText: 'Confirm Password'),
                          obscureText: true,
                          validator: _authMode == AuthMode.signUp
                              ? (value) {
                                  if (value != _passwordController.text) {
                                    return 'Passwords do not match!';
                                  }
                                  return null;
                                }
                              : null,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    ElevatedButton(
                        onPressed: _onSubmit,
                        style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            )),
                        child: Text(
                            _authMode == AuthMode.login ? 'Login' : 'SignUp')),
                    TextButton(
                        onPressed: _onModeChange,
                        child: Text(
                            _authMode == AuthMode.login ? 'SignUp' : 'Login'))
                  ]),
                ),
              ),
            ),
            Visibility(
              visible: _isLoading,
              child: Positioned.fill(
                child: Container(
                  color: Colors.white54,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
