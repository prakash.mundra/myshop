import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/cart_item.dart' as cart_item;
import '../providers/cart.dart' as cart_provider;

class CartItem extends StatelessWidget {
  final cart_item.CartItem cartItem;

  const CartItem(this.cartItem, {super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final productId = cartItem.product.id;
    final price = cartItem.product.price;
    final total = (price * cartItem.quantity);
    return Dismissible(
      key: ValueKey(productId),
      background: Container(
        color: theme.colorScheme.error,
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(right: 20),
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: const Icon(Icons.delete, size: 40, color: Colors.white),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (_) {
        return showDialog(
            context: context,
            builder: (bContext) {
              return AlertDialog(
                title: const Text('Are you sure?'),
                content:
                    const Text('Do you want to remove the item from Cart?'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(bContext).pop(false);
                      },
                      child: const Text('No')),
                  TextButton(
                      onPressed: () {
                        Navigator.of(bContext).pop(true);
                      },
                      child: const Text('Yes')),
                ],
              );
            });
      },
      onDismissed: (_) {
        context.read<cart_provider.Cart>().removeItem(productId);
      },
      child: Card(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: ListTile(
            leading: Chip(
              label: Text('\$$total',
                  style: TextStyle(
                      color: theme.primaryTextTheme.titleLarge?.color)),
              backgroundColor: theme.colorScheme.primary,
            ),
            title: Text(cartItem.product.title),
            subtitle: Text('Price: \$$price'),
            trailing: Text('${cartItem.quantity}x'),
          ),
        ),
      ),
    );
  }
}
