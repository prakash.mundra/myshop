import 'package:flutter/material.dart';

import '../widgets/auth_card.dart';

class Auth extends StatelessWidget {
  const Auth({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              theme.colorScheme.primary.withOpacity(0.2),
              theme.colorScheme.primary.withOpacity(0.6),
            ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
          ),
          Positioned.fill(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: theme.colorScheme.primary,
                      ),
                      transform: Matrix4.rotationZ(-0.4),
                      child: const Text('MyShop',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 36,
                              fontFamily: 'Anton')),
                    ),
                    const AuthCard(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
