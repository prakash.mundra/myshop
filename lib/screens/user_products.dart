import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './edit_product.dart';
import '../providers/products.dart';
import '../widgets/drawer.dart';
import '../widgets/user_product_item.dart';

class UserProducts extends StatefulWidget {
  static const String routeName = '/user_products';

  const UserProducts({super.key});

  @override
  State<UserProducts> createState() => _UserProductsState();
}

class _UserProductsState extends State<UserProducts> {
  var _isLoading = false;
  late Future _productsFuture;

  @override
  void initState() {
    _productsFuture = context.read<Products>().fetchProducts(true);
    super.initState();
  }

  void _showLoading(bool show) {
    setState(() {
      _isLoading = show;
    });
  }

  void _onDelete(BuildContext context, String id) async {
    try {
      _showLoading(true);
      await context.read<Products>().deleteProduct(id);
      _showLoading(false);
    } catch (e) {
      _showLoading(false);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed(EditProduct.routeName);
              },
              icon: const Icon(Icons.add))
        ],
      ),
      drawer: const AppDrawer(),
      body: Stack(children: [
        FutureBuilder(
          future: _productsFuture,
          builder: (bContext, dataSnapshot) {
            if (dataSnapshot.connectionState == ConnectionState.waiting) {
              return Container(
                color: Colors.white54,
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            return RefreshIndicator(
              onRefresh: () => _productsFuture,
              child: Consumer<Products>(
                builder: (bContext, productsProvider, child) {
                  final products = productsProvider.getProducts();
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView.builder(
                      itemBuilder: (_, index) {
                        return Column(
                          children: [
                            UserProductItem(products[index], (String id) {
                              _onDelete(context, id);
                            }),
                            const Divider(),
                          ],
                        );
                      },
                      itemCount: products.length,
                    ),
                  );
                },
              ),
            );
          },
        ),
        if (_isLoading)
          Container(
            color: Colors.white54,
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          )
      ]),
    );
  }
}
