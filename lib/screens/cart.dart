import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/cart.dart' as cart_provider;
import '../providers/orders.dart';
import '../widgets/cart_item.dart';
import '../widgets/my_dialogs.dart';

class Cart extends StatefulWidget {
  static const String routeName = '/cart';

  const Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  var _isLoading = false;

  void _showLoading(bool show) {
    setState(() {
      _isLoading = show;
    });
  }

  Future<void> _onOrder() async {
    final cartProvider = context.read<cart_provider.Cart>();
    final cartItems = cartProvider.getItems().values.toList();
    final total = cartProvider.getTotalAmount();
    try {
      _showLoading(true);
      await context.read<Orders>().addOrder(cartItems, total);
      cartProvider.clear();
      _showLoading(false);
    } catch (e) {
      _showLoading(false);
      MyDialog.showErrorDialog(context, e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final cartProvider = context.watch<cart_provider.Cart>();
    final cartItems = cartProvider.getItems().values.toList();
    final total = cartProvider.getTotalAmount();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Cart'),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Card(
                margin: const EdgeInsets.all(10),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Total',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      const SizedBox(width: 10),
                      Chip(
                        label: Text(
                          '\$$total',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: theme.primaryTextTheme.titleLarge?.color),
                        ),
                        backgroundColor: theme.colorScheme.primary,
                      ),
                      const Spacer(),
                      TextButton(
                        onPressed: total > 0 ? _onOrder : null,
                        style: TextButton.styleFrom(
                            disabledForegroundColor:
                                theme.colorScheme.primary.withOpacity(0.38)),
                        child: const Text(
                          'Order Now',
                          style: TextStyle(fontSize: 18),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: ListView.builder(
                      itemBuilder: (_, index) {
                        return CartItem(cartItems[index]);
                      },
                      itemCount: cartItems.length))
            ],
          ),
          if (_isLoading)
            Container(
              color: Colors.white54,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      ),
    );
  }
}
