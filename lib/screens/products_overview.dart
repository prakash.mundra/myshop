import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/cart.dart' as cart_provider;
import '../providers/products.dart';
import '../screens/cart.dart';
import '../widgets/badge.dart';
import '../widgets/drawer.dart';
import '../widgets/products_grid.dart';

enum FilterOptions { showAll, favourites }

class ProductOverview extends StatefulWidget {
  const ProductOverview({super.key});

  @override
  State<ProductOverview> createState() => _ProductOverviewState();
}

class _ProductOverviewState extends State<ProductOverview> {
  var _showFavourites = false;
  late Future _productsFuture;

  @override
  void initState() {
    _productsFuture = context.read<Products>().fetchProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('MyShop'),
          actions: [
            PopupMenuButton(
                onSelected: (FilterOptions sValue) {
                  setState(() {
                    _showFavourites = (sValue == FilterOptions.favourites);
                  });
                },
                icon: const Icon(Icons.more_vert),
                itemBuilder: (context) {
                  return [
                    const PopupMenuItem(
                        value: FilterOptions.showAll, child: Text('Show All')),
                    const PopupMenuItem(
                        value: FilterOptions.favourites,
                        child: Text('Favourites')),
                  ].toList();
                }),
            Consumer<cart_provider.Cart>(
                builder: (_, cart, ch) {
                  return BadgeWidget(
                      value: '${cart.getItemCount()}', child: ch!);
                },
                child: IconButton(
                  icon: const Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Cart.routeName);
                  },
                ))
          ],
        ),
        drawer: const AppDrawer(),
        body: FutureBuilder(
          future: _productsFuture,
          builder: (_, dataSnapshot) {
            if (dataSnapshot.error != null) {
              return Center(
                  child: Text(
                dataSnapshot.error.toString(),
                textAlign: TextAlign.center,
              ));
            } else {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Container(
                  color: Colors.white54,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              return ProductsGrid(_showFavourites);
            }
          },
        ));
  }
}
