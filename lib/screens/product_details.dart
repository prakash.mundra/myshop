import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';

class ProductDetails extends StatelessWidget {
  static const String routeName = '/product_details';

  const ProductDetails({super.key});

  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context)?.settings.arguments as String;
    final product = context.read<Products>().getProductById(id);
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 300,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
                title: Text(product.title),
                background: Hero(
                  tag: product.id,
                  child: Image.network(product.imageUrl, fit: BoxFit.cover),
                )),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('\$${product.price}',
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                      const SizedBox(height: 100),
                      Text(product.description),
                      const SizedBox(height: 100),
                      Text(product.description),
                      const SizedBox(height: 100),
                      Text(product.description),
                      const SizedBox(height: 100),
                      Text(product.description),
                      const SizedBox(height: 100),
                      Text(product.description),
                      const SizedBox(height: 100),
                      Text(product.description),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
