import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/orders.dart' as orders_provider;
import '../widgets/drawer.dart';
import '../widgets/order_item.dart';

class Orders extends StatelessWidget {
  static const String routeName = '/orders';

  const Orders({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Yours Orders')),
        drawer: const AppDrawer(),
        body: FutureBuilder(
          future: context.read<orders_provider.Orders>().fetchOrders(),
          builder: (_, dataSnapshot) {
            if (dataSnapshot.error != null) {
              return Center(
                  child: Text(
                dataSnapshot.error.toString(),
                textAlign: TextAlign.center,
              ));
            } else {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Container(
                  color: Colors.white54,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              return Consumer<orders_provider.Orders>(
                builder: (context, provider, child) {
                  final orders = provider.getOrders();
                  return ListView.builder(
                    itemBuilder: (_, index) {
                      return OrderItem(orders[index]);
                    },
                    itemCount: orders.length,
                  );
                },
              );
            }
          },
        ));
  }
}
