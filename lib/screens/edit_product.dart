import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/product.dart';
import '../providers/products.dart';
import '../widgets/my_dialogs.dart';

class EditProduct extends StatefulWidget {
  static const String routeName = '/edit_product';

  const EditProduct({super.key});

  @override
  State createState() {
    return _EditProductState();
  }
}

class _EditProductState extends State<EditProduct> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();
  var product = Product(
    id: DateTime.now().toString(),
    title: '',
    description: '',
    price: 0,
    imageUrl: '',
  );
  var _isInit = false;
  var _isLoading = false;

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_onImageUrlFocus);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final id = ModalRoute.of(context)?.settings.arguments;
      if (id != null) {
        product = context.read<Products>().getProductById(id as String);
      }
    }
    _isInit = true;
    _imageUrlController.text = product.imageUrl;
    super.didChangeDependencies();
  }

  bool _isImageUrlValid() {
    final imageUrl = _imageUrlController.text;
    return imageUrl.isNotEmpty && (imageUrl.startsWith('http') || imageUrl.startsWith('https'));
        /*&& (imageUrl.endsWith('.png') ||
            imageUrl.endsWith('.jpg') ||
            imageUrl.endsWith('.jpeg'));*/
  }

  void _onImageUrlFocus() {
    if (!_imageUrlFocusNode.hasFocus && _isImageUrlValid()) {
      setState(() {});
    }
  }

  void _showLoading(bool show) {
    setState(() {
      _isLoading = show;
    });
  }

  void _onSave() async {
    final isValid = _formKey.currentState?.validate();
    if (isValid == true) {
      _formKey.currentState?.save();
      _showLoading(true);
      final productsProvider = context.read<Products>();
      final index = productsProvider.getProductIndexById(product.id);
      try {
        if (index >= 0) {
          await productsProvider.updateProduct(index, product);
        } else {
          await productsProvider.addProduct(product);
        }
        _showLoading(false);
        if (mounted) {
          Navigator.of(context).pop();
        }
      } catch (e) {
        _showLoading(false);
        MyDialog.showErrorDialog(context, e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Product'),
        actions: [IconButton(onPressed: _onSave, icon: const Icon(Icons.save))],
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  TextFormField(
                    initialValue: product.title,
                    decoration: const InputDecoration(labelText: 'Title'),
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(_priceFocusNode);
                    },
                    validator: (value) {
                      return value!.isEmpty ? 'Please enter a title' : null;
                    },
                    onSaved: (value) {
                      product = Product(
                        id: product.id,
                        title: value!,
                        description: product.description,
                        price: product.price,
                        imageUrl: product.imageUrl,
                      );
                    },
                  ),
                  TextFormField(
                    initialValue:
                        product.price > 0 ? product.price.toString() : '',
                    decoration: const InputDecoration(labelText: 'Price'),
                    textInputAction: TextInputAction.next,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    focusNode: _priceFocusNode,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context)
                          .requestFocus(_descriptionFocusNode);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a price';
                      } else if (double.tryParse(value) == null) {
                        return 'Please enter a valid number';
                      } else if (double.parse(value) <= 0) {
                        return 'Please enter a valid number';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      product = Product(
                        id: product.id,
                        title: product.title,
                        description: product.description,
                        price: double.parse(value!),
                        imageUrl: product.imageUrl,
                      );
                    },
                  ),
                  TextFormField(
                    initialValue: product.description,
                    decoration: const InputDecoration(labelText: 'Description'),
                    maxLines: 3,
                    keyboardType: TextInputType.multiline,
                    focusNode: _descriptionFocusNode,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a description';
                      }
                      if (value.length < 10) {
                        return 'Please enter at least 10 characters for description';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      product = Product(
                        id: product.id,
                        title: product.title,
                        description: value!,
                        price: product.price,
                        imageUrl: product.imageUrl,
                      );
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        width: 100,
                        height: 100,
                        margin: const EdgeInsets.only(top: 10, right: 10),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey)),
                        child: _imageUrlController.text.isEmpty
                            ? const Center(
                                child: Text(
                                  'Enter a Url',
                                  textAlign: TextAlign.center,
                                ),
                              )
                            : FittedBox(
                                fit: BoxFit.contain,
                                child: Image.network(_imageUrlController.text),
                              ),
                      ),
                      Expanded(
                        child: TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Image Url'),
                          keyboardType: TextInputType.url,
                          textInputAction: TextInputAction.done,
                          controller: _imageUrlController,
                          focusNode: _imageUrlFocusNode,
                          onFieldSubmitted: (_) {
                            _onSave();
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter an image url';
                            } else if (!value.startsWith('http') ||
                                !value.startsWith('https')) {
                              return 'Please enter a valid image url';
                            }
                            /*else if (!value.endsWith('.png') &&
                                !value.endsWith('.jpg') &&
                                !value.endsWith('.jpeg')) {
                              return 'Please enter a valid image url';
                            }*/
                            return null;
                          },
                          onSaved: (value) {
                            product = Product(
                              id: product.id,
                              title: product.title,
                              description: product.description,
                              price: product.price,
                              imageUrl: value!,
                            );
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          if (_isLoading)
            Container(
              color: Colors.white54,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.dispose();
    _imageUrlFocusNode.removeListener(_onImageUrlFocus);
    super.dispose();
  }
}
